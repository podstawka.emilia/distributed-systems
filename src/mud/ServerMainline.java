package mud;

import java.rmi.RemoteException;


public class ServerMainline
{
    public static void main(String[] args) throws RemoteException
    {
        if (args.length < 2) {
            System.err.println( "Usage:\njava ServerMainline <registryport> <serverport>" ) ;
            return;
        }
        int registryport = Integer.parseInt( args[0] ) ;
        int serverport = Integer.parseInt( args[1] ) ;
        ServerImpl server = new ServerImpl(registryport, serverport);
    }
}
