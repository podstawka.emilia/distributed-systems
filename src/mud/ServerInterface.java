package mud;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote {
    //void notification(String msg, boolean error) throws RemoteException;
    String setPlayerStartLocation(ClientImpl client) throws RemoteException;
    String getPlayerLook(ClientImpl client) throws RemoteException;
    String movePlayer(ClientImpl client, String direction) throws RemoteException;
    String takeItem(ClientImpl client, String command) throws RemoteException;
}
