package mud;

import java.rmi.RemoteException;

public class ClientMainline {
    public static void main(String[] args) throws RemoteException
    {
        if (args.length < 3) {
            System.err.println( "Usage:\njava ClientMainline <registryhost> <registryport> <serverport>" ) ;
            return;
        }
        String hostname = args[0];
        int registryport = Integer.parseInt( args[1] ) ;
        int serverport = Integer.parseInt( args[2] ) ;

        ClientImpl client = new ClientImpl(registryport, serverport, hostname);
    }
}
