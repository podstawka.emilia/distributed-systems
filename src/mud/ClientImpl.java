package mud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

class ClientImpl implements ClientInterface, Serializable {
    String mudGame = "default"; //change later this is name boi
    Boolean playing = false;
    String username;
    String location;
    List<String> inventory = new ArrayList<>();
    ServerInterface remote;
    private void gameLoop() throws RemoteException {
        this.location = this.remote.setPlayerStartLocation(this);
        this.playing = true;


        while(playing) {
            String command = this.enterCommand();

            if(command.equals("look"))
                System.out.println(
                        this.remote.getPlayerLook(this)
                );
            if(command.matches("east|north|west|south")){
                this.location = this.remote.movePlayer(this, command);
                System.out.println("You went " + command
                );
            }
            if(command.startsWith("take ")) {
                command = command.replace("take ", "");
                String item = this.remote.takeItem(this, command);
                if(item.equals("nothing"))
                System.out.println("You can't pick up what's not there... Nice try");
                else{
                    System.out.println("You took " + item);
                    this.inventory.add(item);
                    }
            }
            if(command.equals("inventory")){
                if(inventory.isEmpty()) System.out.println("Your inventory is empty");
                else System.out.println("You have " + inventory + " in your inventory.");
            }

        }
    }

    private String enterCommand() {
        System.out.print(this.username + "-->");

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in)); // char by char input

        try { return input.readLine(); }
        catch (IOException e) { return e.getMessage(); }
    }

    private String enterName() {
        System.out.print("Enter your name: ");

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in)); // char by char input

        try { return input.readLine(); }
        catch (IOException e) { return e.getMessage(); }
    }

    ClientImpl(int registryport, int serverport, String hostname) throws RemoteException{
        try {

            System.setProperty( "java.security.policy", "mud.policy" ) ;
            System.setSecurityManager( new SecurityManager() ) ;

            String regURL = "rmi://" + hostname + ":" + registryport + "/MUD";
            this.remote = (ServerInterface)Naming.lookup( regURL );

            this.username = this.enterName(); // make unique later

            this.gameLoop();
        }
        catch(java.rmi.NotBoundException e) {
            System.err.println( "Can't find the server in the registry." );
        }
        catch (java.io.IOException e) {
            System.out.println( "Failed to register." );
        }
    }
}

