package mud; /***********************************************************************
 * cs3524.solutions.mud.Vertex
 ***********************************************************************/

import java.util.*;

// Represents a location in the MUD (a vertex in the graph).
class Vertex
{
    public String _name;             // Vertex name
    public String _msg = "";         // Message about this location
    public Map<String,Edge> _routes; // Association between direction
				     // (e.g. "north") and a path
				     // (Edge)
    public List<String> _things;     // The things (e.g. players) at
				     // this location
	public List<String> _players;

    public Vertex( String nm )
    {
	_name = nm; 
	_routes = new HashMap<String,Edge>(); // Not synchronised
	_things = new Vector<String>();       // Synchronised
	_players = new ArrayList<>();
    }

    public String toString()
    {
	String summary = "\n";
	summary += _msg + "\n";
	Iterator iter = _routes.keySet().iterator();
	String direction;
	while (iter.hasNext()) {
	    direction = (String)iter.next();
	    summary += "To the " + direction + " there is " + ((Edge)_routes.get( direction ))._view + "\n";
	}
	iter = _things.iterator();
	Iterator users = _players.iterator();
	if (iter.hasNext()) {
	    summary += "You can see those things: ";
	    do {
		summary += iter.next() + " ";
	    } while (iter.hasNext());
	}
		summary += "\n";
	if (users.hasNext()){
		summary += "You can see those users: ";
		do {
			summary += users.next() + " ";
		} while (users.hasNext());
	}
	summary += "\n\n";
	return summary;
    }
}

