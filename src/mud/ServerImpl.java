package mud;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class ServerImpl implements ServerInterface {
    private String hostname;
    private Map<String, MUD> gameMUDs = new HashMap<>(); // good

    public String setPlayerStartLocation(ClientImpl client) {

        MUD currentMUD = this.gameMUDs.get(client.mudGame);
        String startLoc = currentMUD.startLocation();
        currentMUD.addPlayer(startLoc, client.username);

        return startLoc;
    }


    public String getPlayerLook(ClientImpl client) {
        String msg;
        MUD currentMUD = this.gameMUDs.get(client.mudGame);
        msg = currentMUD.locationInfo(client.location);

        return msg;
    }

    public String movePlayer(ClientImpl client, String direction){
        MUD currentMUD = this.gameMUDs.get(client.mudGame);
        String newLocation = currentMUD.moveThing(client.location, direction, client.username);
        return newLocation;
    }
    public String takeItem(ClientImpl client, String item){
        MUD currentMUD = this.gameMUDs.get(client.mudGame);
        if(currentMUD.thingExists(client, item)) {
            currentMUD.delThing(client.location, item);
            return item;
        }
        else return "nothing";
    }
    private void createMUDInstance(String mud_name) {
        String edges = "./mymud.edg";
        String messages = "./mymud.msg";
        String things = "./mymud.thg";

        this.gameMUDs.put(mud_name, new MUD(edges, messages, things));

    }

    ServerImpl(int registryport, int serverport) throws RemoteException {
        LocateRegistry.createRegistry(registryport);

        //create server
        try {
            this.hostname = (InetAddress.getLocalHost()).getCanonicalHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        System.setProperty( "java.security.policy", "mud.policy") ;
        System.setSecurityManager( new SecurityManager() ) ;

        ServerInterface stub = (ServerInterface) UnicastRemoteObject.exportObject( this, serverport );
        try {
            Naming.rebind( "rmi://" + this.hostname +":" + registryport + "/MUD", stub );
            System.out.println("hostname: " + this.hostname +
                    "\nrmiport: " + registryport);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        this.createMUDInstance("default");

    }
}